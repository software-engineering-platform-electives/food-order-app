# Food Order App

## Setup

In the project directory, run:

### `npm install`

Installs all dependenices needed for this project.

## Running Script

In the project directory, run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## App Usage

- view list of meals, its descriptions, and corresponding prices
- adjust amount then click + Add button
- view your cart by clicking the button on the header
- click - or + buttons to remove or add more items to cart
- view total amount of order or close
- click order, and fill up the user details form
- click confirm to send order to database, or cancel to close 

## Other Notes
- this app uses firebase console